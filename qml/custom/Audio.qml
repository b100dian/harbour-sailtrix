import QtQuick 2.0
import Messages 1.0
import QtMultimedia 5.6
import Sailfish.Silica 1.0

Row {
    property string path;

    height: playPauseButton.height
    width : playPauseButton.width + p.width

    MediaPlayer {
        id: player
        autoPlay: false
        source: path
        autoLoad: false
        onDurationChanged: {
            var secondsA = Math.floor(duration / 1000)
            var date = new Date(null);
            if (secondsA < 0)
                secondsA = 0
            date.setSeconds(secondsA);
            var durationString = date.toISOString().substr(11, 8);
            if (durationString.indexOf("00") === 0)
                durationString = durationString.substr(3)
            durationLabel.text = durationString
        }
    }

    IconButton {
        id: playPauseButton
        icon.source: "image://theme/icon-l-" + (player.playbackState === MediaPlayer.PlayingState ? "pause" : "play")
        onClicked: {
            if (player.playbackState === MediaPlayer.StoppedState || player.playbackState == MediaPlayer.PausedState) {
                player.play();
            } else {
                player.pause();
            }
        }

        height: Theme.iconSizeMedium
        width: Theme.iconSizeMedium
        icon.height: Theme.iconSizeMedium
        icon.width: Theme.iconSizeMedium
    }

    Label {
        id: durationLabel
    }

    Slider {
        id: p
        enabled: player.seekable
        maximumValue: player.duration
        value: player.position
        onReleased: {
            player.seek(value);
            value = Qt.binding(function() { return player.position })
        }
        width: parent.width - playPauseButton.width
    }
}
