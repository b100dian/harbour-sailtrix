#ifndef SAILTRIXADAPTER_H
#define SAILTRIXADAPTER_H

#include <QDBusAbstractAdaptor>

class SailtrixAdapter : public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.yeheng.sailtrix")
public:
    SailtrixAdapter(QObject* parent);
public slots:
    Q_NOREPLY void open();
    Q_NOREPLY void openWithUrl(const QString& url);
    Q_NOREPLY void showRoom(const QString& room_id);
};

#endif // SAILTRIXADAPTER_H
